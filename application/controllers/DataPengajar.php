<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DataPengajar extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('PengajarModel');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('security');
	}
	
	public function index()
	{
		$def['title'] = 'Dashboard';

		// Menu 

		$menu = $this->RolesMenusModel->get_menu();
		$def['menu'] = fetch_menu($menu);

		// BreadCrumb

		// $this->mybreadcrumb->add('<i class="icofont-ui-home"></i>', base_url(''));
		// $def['breadcrumb'] = $this->mybreadcrumb->render();

		// Page Plugins
		$def['css'] = '
		<link rel="stylesheet" type="text/css" href="'. site_url('assets/css/animate.css').'">
		<link rel="stylesheet" type="text/css" href="'. site_url('assets/css/datatables.css').'">
		';

		$this->load->view('partials/head', $def);
		$this->load->view('partials/header');
		$this->load->view('partials/sidebar', $def);
		$this->load->view('pages/datapengajar');
		$this->load->view('partials/footer');
		$this->load->view('partials/plugins');
		$this->load->view('pages/plugins/datapengajar');
	}

	public function getDatatables()
	{
		$list = $this->PengajarModel->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $ls) {

			if ($ls->foto == NULL) {
				$foto = '<a href="'.site_url('assets/images/pengajar/default.png').'" target="_blank"><img src="'.site_url('assets/images/pengajar/default.png').'" width="30" height="30"></a>';
			}else{
				$foto = '<a href="'.site_url('assets/images/pengajar/'.$ls->foto).'" target="_blank"><img src="'.site_url('assets/images/pengajar/'.$ls->foto).'" width="30" height="30"></a>';
			}

			if ($ls->status == 0) {
				$status = '<span class="badge rounded-pill badge-danger">Not Active</span>';
			}if ($ls->status == 1) {
				$status = '<span class="badge rounded-pill badge-success">Active</span>';
			}

			$no++;
			$row = array();
			$row[] = $foto;
			$row[] = $ls->nip;
			$row[] = $ls->nama_lengkap;
			$row[] = $ls->email;
			$row[] = $ls->jenis_kelamin;
			$row[] = $ls->hp;
			$row[] = $status;

			$row[] = '
			<div class="btn-group btn-group-sm btn-group-pill" role="group" aria-label="Basic example">
			<button class="btn btn-outline-primary" type="button" title="View"><i class="icofont icofont-eye-alt"></i></button>
			<button class="btn btn-outline-primary" type="button" title="Update"><i class="icofont icofont-settings"></i></button>
			<button class="btn btn-outline-primary" type="button" title="Delete"><i class="icofont icofont-delete-alt"></i></button>
			</div>
			';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->PengajarModel->count_all(),
			"recordsFiltered" => $this->PengajarModel->count_filtered(),
			"data" => $data
		);

		echo json_encode($output);
	}

	public function create()
	{
		$config = array(
			array(

				'field' => 'nama_lengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required|xss_clean',
				'errors' => array(
					'required' => 'Nama Lengkap Wajib diisi',
				),

			),

			array(

				'field' => 'nip',
				'label' => 'NIP',
				'rules' => 'required|trim|min_length[6]|xss_clean',
				'errors' => array(
					'required' => 'NIP Wajib diisi',
					'trim' => 'NIP tidak boleh menggunakan spasi',
					'min_length' => 'NIP minimal 6 karakter',
				),

			),

			array(

				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email|is_unique[pengajar.email]|xss_clean',
				'errors' => array(
					'required' => 'Email Wajib diisi',
					'valid_email' => 'Harap isi email dengan benar',
					'is_unique' => 'Email sudah tersedia',
				),
			),

			array(

				'field' => 'jenis_kelamin',
				'label' => 'Jenis Kelamin',
				'rules' => 'required|xss_clean',
				'errors' => array(
					'required' => 'Jenis Kelamin Wajib diisi',
				),
			),

		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {

			$data = [
				'type'              => 'val_error',
				'nama_lengkap'      => form_error('nama_lengkap', '<small class="txt-danger">', '</small>'),
				'nip'          		=> form_error('nip', '<small class="txt-danger">', '</small>'),
				'email'             => form_error('email', '<small class="txt-danger">', '</small>'),
				'jenis_kelamin'		=> form_error('jenis_kelamin', '<small class="txt-danger">', '</small>')
			];

			echo json_encode($data);
		} else {
			$this->load->library('upload');
	        $nmfile = "png_".time();
	        $config['upload_path'] = './assets/uploads/';
	        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
	        $config['max_size'] = '3072';
	        $config['max_width']  = '5000';
	        $config['max_height']  = '5000';
	        $config['file_name'] = $nmfile;

			$this->upload->initialize($config);
			
			if($_FILES['foto']['name'])
	        {
	            if ($this->upload->do_upload('foto'))
	            {
	                $gbr = $this->upload->data();
	                $data['foto'] = $gbr['file_name'];
	            }else{
	            	$response = array(
						'type' => 'warning',
						'title' => 'Gagal !',
						'message' => 'Foto gagal di upload !',
						// 'redirect' => base_url('login'),
					);
	            }
	        }

			$data['nama_lengkap']     	= $this->input->post('nama_lengkap');
			$data['nip']         		= $this->input->post('nip');
			$data['email']				= $this->input->post('email');
			$data['jenis_kelamin']		= $this->input->post('jenis_kelamin');

			$act = $this->PengajarModel->create($data);
			if ($act) {
				$response = array(
					'type' => 'success',
					'title' => 'Berhasil !',
					'message' => 'Data berhasil di input !',
					// 'redirect' => base_url('login'),
				);
			} else {
				$response = array(
					'type' => 'danger',
					'title' => 'Gagal !',
					'message' => 'Data gagal di input !',
					// 'redirect' => base_url('register'),
				);
			}

			echo json_encode($response);
		}

	}
	
}

/* End of file DataPengajar.php */
/* Location: ./application/controllers/DataPengajar.php */
?>