
<script src="<?= site_url('assets/js/datatable/datatables/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#table-pengajar').DataTable({
            "processing": true, 
            "serverSide": true,
            // "scrollX": true,
            // "fixedColumns": {
            //   "leftColumns": 1,
            //   "rightColumns": 2
            // },
            "responsive": true,
            // "lengthChange": false,
            "order": [],
            "autoWidth" : true,

            "ajax": {
                "url": "<?= base_url('data-pengajar/getDatatables')?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "2%"
                },
                // {
                //     "targets": 2,
                //     "className": "text-right",
                // }
            ],
            "language": {
                "paginate": {
                    "previous": '<i class="icofont icofont-caret-left"></i>',
                    "next": '<i class="icofont icofont-caret-right"></i>'
                },
                "aria": {
                    "paginate": {
                        "previous": 'Previous',
                        "next":     'Next'
                    }
                },
                "search": "",
                "searchPlaceholder": "Search . . .",
                "lengthMenu":"_MENU_",
            }
        });

        function reload_table() {
            table.ajax.reload(null, false);
        }

        $('#form-add-pengajar').on('submit', function() {

            $.ajax({
                url: '<?= base_url('data-pengajar/create') ?>',
                type: 'POST',
                dataType: 'JSON',
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,

                success: function(response) {
                    if (response.type == 'val_error') {
                        $('.invalid-feedback-nama_lengkap').html(response.nama_lengkap);
                        $('.invalid-feedback-nip').html(response.nip);
                        $('.invalid-feedback-email').html(response.email);
                        $('.invalid-feedback-jenis_kelamin').html(response.jenis_kelamin);
                    } else {
                        notification(response.type, response.title, response.message)
                        reload_table();
                    }
                }
            });

            return false;
        });
    });
    
</script>
<!-- Theme js-->
<script src="<?= site_url('assets/js/script.js') ?>"></script>
<script src="<?= site_url('assets/js/theme-customizer/customizer.js') ?>"></script>
<!-- login js-->
<!-- Plugin used-->
</body>
</html>