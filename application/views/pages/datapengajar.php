
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">

      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Data Pengajar</h5>
            <div class="setting-list">
              <ul class="list-unstyled setting-option">
                <li>
                  <div class="setting-primary"><i class="icon-settings"></i></div>
                </li>
                <li id="addModal" data-bs-toggle="modal" data-bs-target="#modal-add-pengajar"><i class="fa fa-plus font-primary"></i></li>
                <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                <li><i class="icofont icofont-error close-card font-primary"></i></li>
              </ul>
            </div>
            <span>table dibawah ini adalah data pengajar yang ada diaplikasi ini.</span>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="display horizontal-scroll" id="table-pengajar">
                <thead>
                  <tr>
                    <th>Foto</th>
                    <th>NIP</th>
                    <th>Nama Lengkap</th>
                    <th>Email</th>
                    <th>Jenis Kelamin</th>
                    <th>HP</th>
                    <th>Status</th>
                    <th>Opsi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modal-add-pengajar" tabindex="-1" role="dialog" aria-labelledby="modal-add-pengajar" aria-hidden="true">
  <div class="modal-dialog modal-dialog-top" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Data</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" id="form-add-pengajar" enctype="multipart/form-data">

          <div class="row">

            <div class="col-md">
              <div class="mb-3">
                <label class="col-form-label" for="nip">NIP</label>
                <input class="form-control" id="nip" type="text" name="nip">
                <div class="invalid-feedback-nip"></div>
              </div>
            </div>

            <div class="col-md">
              <div class="mb-3">
                <label class="col-form-label" for="nama-lengkap">Nama Lengkap</label>
                <input class="form-control" id="nama-lengkap" type="text" name="nama_lengkap">
                <div class="invalid-feedback-nama_lengkap"></div>
              </div>
            </div>

          </div>

          <div class="row">

            <div class="col-md">
              <div class="mb-3">
                <label class="col-form-label" for="email">Email</label>
                <input class="form-control" id="email" type="text" name="email">
                <div class="invalid-feedback-email"></div>
              </div>
            </div>

            <div class="col-md">
              <div class="mb-3">
                <label class="col-form-label" for="jenis_kelamin">Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control">
                  <option value="Wanita">Perempuan</option>
                  <option value="Pria">Laki-Laki</option>
                </select>
                <div class="invalid-feedback-jenis_kelamin"></div>
              </div>
            </div>

          </div>
          
          <!-- <div class="row"> -->
            <label>Upload Foto</label>
            <input class="form-control" type="file" name="foto">
            <div class="invalid-feedback-foto"></div>

            <!-- </div> -->

          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
          <button class="btn btn-primary" type="submit" form="form-add-pengajar">Save</button>
        </div>
      </div>
    </div>
  </div>