
<!-- latest jquery-->
<script src="<?= site_url('assets/js/jquery-3.5.1.min.js') ?>"></script>
<!-- feather icon js-->
<script src="<?= site_url('assets/js/icons/feather-icon/feather.min.js') ?>"></script>
<script src="<?= site_url('assets/js/icons/feather-icon/feather-icon.js') ?>"></script>
<!-- Sidebar jquery-->
<script src="<?= site_url('assets/js/sidebar-menu.js') ?>"></script>
<script src="<?= site_url('assets/js/config.js') ?>"></script>
<!-- Plugins JS start-->
<!-- Bootstrap js-->
<script src="<?= site_url('assets/js/bootstrap/popper.min.js') ?>"></script>
<script src="<?= site_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= site_url('assets/js/prism/prism.min.js') ?>"></script>
<script src="<?= site_url('assets/js/clipboard/clipboard.min.js') ?>"></script>
<script src="<?= site_url('assets/js/custom-card/custom-card.js') ?>"></script>